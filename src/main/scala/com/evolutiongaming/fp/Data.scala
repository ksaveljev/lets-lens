package com.evolutiongaming.fp

import cats.kernel.Monoid
import cats.{Applicative, Functor}
import cats.syntax.functor._

object Data {

  case class Locality(city: String, state: String, country: String)
  case class Address(street: String, suburb: String, locality: Locality)
  case class Person(age: Int, name: String, address: Address)
  case class IntAnd[A](int: Int, a: A)

  sealed trait IntOr[+A]
  case class IntOrIs(int: Int) extends IntOr[Nothing]
  case class IntOrIsNot[A](a: A) extends IntOr[A]

  val fredLocality: Locality =
    Locality("Fredmania", "New South Fred", "Fredalia")

  val fredAddress: Address =
    Address("15 Fred St", "Fredville", fredLocality)

  val fred: Person =
    Person(24, "Fred", fredAddress)

  val maryLocality: Locality =
    Locality("Mary Mary", "Western Mary", "Maristan")

  val maryAddress: Address =
    Address("83 Mary Ln", "Maryland", maryLocality)

  val mary: Person =
    Person(28, "Mary", maryAddress)



  case class Store[S, A](fs: S => A, s: S)


  case class Const[A, B](getConst: A)

  implicit def constFunctor[A]: Functor[Const[A, ?]] = new Functor[Const[A, ?]] {
    def map[B, C](fa: Const[A, B])(f: B => C): Const[A, C] = Const(fa.getConst)
  }

  implicit def constApplicative[A](implicit ev: Monoid[A]): Applicative[Const[A, ?]] = new Applicative[Const[A, ?]] {
    def pure[B](x: B): Const[A, B] = Const(ev.empty)
    def ap[B, C](ff: Const[A, B => C])(fa: Const[A, B]): Const[A, C] = Const(ev.combine(ff.getConst, fa.getConst))
  }


  case class Tagged[A, B](getTagged: B)

  implicit def taggedFunctor[A]: Functor[Tagged[A, ?]] = new Functor[Tagged[A, ?]] {
    def map[B, C](fa: Tagged[A, B])(f: B => C): Tagged[A, C] = Tagged(f(fa.getTagged))
  }

  implicit def taggedApplicative[A]: Applicative[Tagged[A, ?]] = new Applicative[Tagged[A, ?]] {
    def pure[B](x: B): Tagged[A, B] = Tagged(x)
    def ap[B, C](ff: Tagged[A, B => C])(fa: Tagged[A, B]): Tagged[A, C] = Tagged(ff.getTagged(fa.getTagged))
  }


  case class Identity[A](getIdentity: A)

  implicit val identityFunctor: Functor[Identity] = new Functor[Identity] {
    def map[A, B](fa: Identity[A])(f: A => B): Identity[B] = Identity(f(fa.getIdentity))
  }

  implicit val identityApplicative: Applicative[Identity] = new Applicative[Identity] {
    def pure[A](x: A): Identity[A] = Identity(x)
    def ap[A, B](ff: Identity[A => B])(fa: Identity[A]): Identity[B] = Identity(ff.getIdentity(fa.getIdentity))
  }


  case class AlongsideLeft[F[_], B, A](getAlongsideLeft: F[(A, B)])

  implicit def alongsideLeftFunctor[F[_]: Functor, B]: Functor[AlongsideLeft[F, B, ?]] = new Functor[AlongsideLeft[F, B, ?]] {
    def map[X, Y](fa: AlongsideLeft[F, B, X])(f: X => Y): AlongsideLeft[F, B, Y] =
      AlongsideLeft(fa.getAlongsideLeft.map(x => (f(x._1), x._2)))
  }


  case class AlongsideRight[F[_], A, B](getAlongsideRight: F[(A, B)])

  implicit def alongsideRightFunctor[F[_]: Functor, A]: Functor[AlongsideRight[F, A, ?]] = new Functor[AlongsideRight[F, A, ?]] {
    def map[X, Y](fa: AlongsideRight[F, A, X])(f: X => Y): AlongsideRight[F, A, Y] =
      AlongsideRight(fa.getAlongsideRight.map(x => (x._1, f(x._2))))
  }



  def bool[A](f: A)(t: A)(b: Boolean): A = if (b) t else f
}
