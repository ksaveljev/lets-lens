package com.evolutiongaming.fp

import cats.Functor
import cats.syntax.functor._
import com.evolutiongaming.fp.Data.{Address, Locality, Person}

object GetSetLens {

  case class Lens[A, B](set: A => B => A, get: A => B)

  // |
  //
  // >>> get fstL (0 :: Int, "abc")
  // 0
  //
  // >>> get sndL ("abc", 0 :: Int)
  // 0
  //
  // prop> let types = (x :: Int, y :: String) in get fstL (x, y) == x
  //
  // prop> let types = (x :: Int, y :: String) in get sndL (x, y) == y
  def get[A, B](lens: Lens[A, B])(a: A): B =
    lens.get(a)

  // |
  //
  // >>> set fstL (0 :: Int, "abc") 1
  // (1,"abc")
  //
  // >>> set sndL ("abc", 0 :: Int) 1
  // ("abc",1)
  //
  // prop> let types = (x :: Int, y :: String) in set fstL (x, y) z == (z, y)
  //
  // prop> let types = (x :: Int, y :: String) in set sndL (x, y) z == (x, z)
  def set[A, B](lens: Lens[A, B])(a: A)(b: B): A =
    lens.set(a)(b)

  // The get/set law of lenses. This function should always return True
  def getsetLaw[A, B](lens: Lens[A, B])(a: A): Boolean =
    set(lens)(a)(get(lens)(a)) == a

  // The set/get law of lenses. This function should always return True
  def setgetLaw[A, B](lens: Lens[A, B])(a: A)(b: B): Boolean =
    get(lens)(set(lens)(a)(b)) == b

  // The set/set law of lenses. This function should always return True
  def setsetLaw[A, B](lens: Lens[A, B])(a: A)(b1: B)(b2: B): Boolean =
    set(lens)(set(lens)(a)(b1))(b2) == set(lens)(a)(b2)

  //////////

  // |
  //
  // >>> modify fstL (+1) (0 :: Int, "abc")
  // (1,"abc")
  //
  // >>> modify sndL (+1) ("abc", 0 :: Int)
  // ("abc",1)
  //
  // prop> let types = (x :: Int, y :: String) in modify fstL id (x, y) == (x, y)
  //
  // prop> let types = (x :: Int, y :: String) in modify sndL id (x, y) == (x, y)
  def modify[A, B](lens: Lens[A, B])(f: B => B)(a: A): A =
    sys.error("todo: modify")

  // |
  //
  // >>> fstL .~ 1 $ (0 :: Int, "abc")
  // (1,"abc")
  //
  // >>> sndL .~ 1 $ ("abc", 0 :: Int)
  // ("abc",1)
  //
  // prop> let types = (x :: Int, y :: String) in set fstL (x, y) z == (fstL .~ z $ (x, y))
  //
  // prop> let types = (x :: Int, y :: String) in set sndL (x, y) z == (sndL .~ z $ (x, y))
  def `.~`[A, B](lens: Lens[A, B])(b: B)(a: A): A =
    sys.error("todo: `.~`")

  // |
  //
  // >>> fmodify fstL (+) (5 :: Int, "abc") 8
  // (13,"abc")
  //
  // >>> fmodify fstL (\n -> bool Nothing (Just (n * 2)) (even n)) (10, "abc")
  // Just (20,"abc")
  //
  // >>> fmodify fstL (\n -> bool Nothing (Just (n * 2)) (even n)) (11, "abc")
  // Nothing
  def fmodify[F[_]: Functor, A, B](lens: Lens[A, B])(f: B => F[B])(a: A): F[A] =
    sys.error("todo: fmodify")

  // |
  //
  // >>> fstL |= Just 3 $ (7, "abc")
  // Just (3,"abc")
  //
  // >>> (fstL |= (+1) $ (3, "abc")) 17
  // (18,"abc")
  def |=[F[_]: Functor, A, B](lens: Lens[A, B])(fb: F[B])(a: A): F[A] =
    sys.error("todo: (|=)")

  // |
  //
  // >>> modify fstL (*10) (3, "abc")
  // (30,"abc")
  //
  // prop> let types = (x :: Int, y :: String) in getsetLaw fstL (x, y)
  //
  // prop> let types = (x :: Int, y :: String) in setgetLaw fstL (x, y) z
  //
  // prop> let types = (x :: Int, y :: String) in setsetLaw fstL (x, y) z
  def fstL[A, B]: Lens[(A, B), A] =
    sys.error("todo: fstL")

  // |
  //
  // >>> modify sndL (++ "def") (13, "abc")
  // (13,"abcdef")
  //
  // prop> let types = (x :: Int, y :: String) in getsetLaw sndL (x, y)
  //
  // prop> let types = (x :: Int, y :: String) in setgetLaw sndL (x, y) z
  //
  // prop> let types = (x :: Int, y :: String) in setsetLaw sndL (x, y) z
  def sndL[A, B]: Lens[(A, B), B] =
    sys.error("todo: sndL")

  // |
  //
  // >>> get (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d']))
  // Just 'c'
  //
  // >>> get (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d']))
  // Nothing
  //
  // >>> set (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) (Just 'X')
  // fromList [(1,'a'),(2,'b'),(3,'X'),(4,'d')]
  //
  // >>> set (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) (Just 'X')
  // fromList [(1,'a'),(2,'b'),(3,'c'),(4,'d'),(33,'X')]
  //
  // >>> set (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) Nothing
  // fromList [(1,'a'),(2,'b'),(4,'d')]
  //
  // >>> set (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) Nothing
  // fromList [(1,'a'),(2,'b'),(3,'c'),(4,'d')]
  def mapL[K, V](key: K): Lens[Map[K, V], Option[V]] =
    sys.error("todo: mapL")

  // |
  //
  // >>> get (setL 3) (Set.fromList [1..5])
  // True
  //
  // >>> get (setL 33) (Set.fromList [1..5])
  // False
  //
  // >>> set (setL 3) (Set.fromList [1..5]) True
  // fromList [1,2,3,4,5]
  //
  // >>> set (setL 3) (Set.fromList [1..5]) False
  // fromList [1,2,4,5]
  //
  // >>> set (setL 33) (Set.fromList [1..5]) True
  // fromList [1,2,3,4,5,33]
  //
  // >>> set (setL 33) (Set.fromList [1..5]) False
  // fromList [1,2,3,4,5]
  def setL[K](key: K): Lens[Set[K], Boolean] =
    sys.error("todo: setL")

  // |
  //
  // >>> get (compose fstL sndL) ("abc", (7, "def"))
  // 7
  //
  // >>> set (compose fstL sndL) ("abc", (7, "def")) 8
  // ("abc",(8,"def"))
  def compose[A, B, C](lens1: Lens[B, C])(lens2: Lens[A, B]): Lens[A, C] =
    sys.error("todo: compose")

  // |
  //
  // >>> get identity 3
  // 3
  //
  // >>> set identity 3 4
  // 4
  def identity[A]: Lens[A, A] =
    sys.error("todo: identity")

  // |
  //
  // >>> get (product fstL sndL) (("abc", 3), (4, "def"))
  // ("abc","def")
  //
  // >>> set (product fstL sndL) (("abc", 3), (4, "def")) ("ghi", "jkl")
  // (("ghi",3),(4,"jkl"))
  def product[A, B, C, D](lens1: Lens[A, B])(lens2: Lens[C, D]): Lens[(A, C), (B, D)] =
    sys.error("todo: product")

  // |
  //
  // >>> get (choice fstL sndL) (Left ("abc", 7))
  // "abc"
  //
  // >>> get (choice fstL sndL) (Right ("abc", 7))
  // 7
  //
  // >>> set (choice fstL sndL) (Left ("abc", 7)) "def"
  // Left ("def",7)
  //
  // >>> set (choice fstL sndL) (Right ("abc", 7)) 8
  // Right ("abc",8)
  def choice[A, B, X](lens1: Lens[A, X])(lens2: Lens[B, X]): Lens[Either[A, B], X] =
    sys.error("todo: choice")

  //////////

  val cityL: Lens[Locality, String] =
    Lens(l => c => Locality(c, l.state, l.country), l => l.city)

  val stateL: Lens[Locality, String] =
    Lens(l => s => Locality(l.city, s, l.country), l => l.state)

  val countryL: Lens[Locality, String] =
    Lens(l => c => Locality(l.city, l.state, c), l => l.country)

  val streetL: Lens[Address, String] =
    Lens(a => s => Address(s, a.suburb, a.locality), a => a.street)

  val suburbL: Lens[Address, String] =
    Lens(a => s => Address(a.street, s, a.locality), a => a.suburb)

  val localityL: Lens[Address, Locality] =
    Lens(a => l => Address(a.street, a.suburb, l), a => a.locality)

  val ageL: Lens[Person, Int] =
    Lens(p => a => Person(a, p.name, p.address), p => p.age)

  val nameL: Lens[Person, String] =
    Lens(p => n => Person(p.age, n, p.address), p => p.name)

  val addressL: Lens[Person, Address] =
    Lens(p => a => Person(p.age, p.name, a), p => p.address)


  // |
  //
  // >>> getSuburb fred
  // "Fredville"
  //
  // >>> getSuburb mary
  // "Maryland"
  def getSuburb: Person => String =
    sys.error("todo: getSuburb")

  // |
  //
  // >>> setStreet fred "Some Other St"
  // Person 24 "Fred" (Address "Some Other St" "Fredville" (Locality "Fredmania" "New South Fred" "Fredalia"))
  //
  // >>> setStreet mary "Some Other St"
  // Person 28 "Mary" (Address "Some Other St" "Maryland" (Locality "Mary Mary" "Western Mary" "Maristan"))
  def setStreet(person: Person)(street: String): Person =
    sys.error("todo: setStreet")

  // |
  //
  // >>> getAgeAndCountry (fred, maryLocality)
  // (24,"Maristan")
  //
  // >>> getAgeAndCountry (mary, fredLocality)
  // (28,"Fredalia")
  def getAgeAndCountry(pl: (Person, Locality)): (Int, String) =
    sys.error("todo: getAgeAndCountry")

  // |
  //
  // >>> setCityAndLocality (fred, maryAddress) ("Some Other City", fredLocality)
  // (Person 24 "Fred" (Address "15 Fred St" "Fredville" (Locality "Some Other City" "New South Fred" "Fredalia")),Address "83 Mary Ln" "Maryland" (Locality "Fredmania" "New South Fred" "Fredalia"))
  //
  // >>> setCityAndLocality (mary, fredAddress) ("Some Other City", maryLocality)
  // (Person 28 "Mary" (Address "83 Mary Ln" "Maryland" (Locality "Some Other City" "Western Mary" "Maristan")),Address "15 Fred St" "Fredville" (Locality "Mary Mary" "Western Mary" "Maristan"))
  def setCityAndLocality(pa: (Person, Address))(cityAndLocality: (String, Locality)): (Person, Address) =
    sys.error("todo: setCityAndLocality")

  // |
  //
  // >>> getSuburbOrCity (Left maryAddress)
  // "Maryland"
  //
  // >>> getSuburbOrCity (Right fredLocality)
  // "Fredmania"
  def getSuburbOrCity(addressOrLocality: Either[Address, Locality]): String =
    sys.error("todo: getSuburbOrCity")

  // |
  //
  // >>> setStreetOrState (Right maryLocality) "Some Other State"
  // Right (Locality "Mary Mary" "Some Other State" "Maristan")
  //
  // >>> setStreetOrState (Left fred) "Some Other St"
  // Left (Person 24 "Fred" (Address "Some Other St" "Fredville" (Locality "Fredmania" "New South Fred" "Fredalia")))
  def setStreetOrState(personOrLocality: Either[Person, Locality])(s: String): Either[Person, Locality] =
    sys.error("todo: setStreetOrState")

  // |
  //
  // >>> modifyCityUppercase fred
  // Person 24 "Fred" (Address "15 Fred St" "Fredville" (Locality "FREDMANIA" "New South Fred" "Fredalia"))
  //
  // >>> modifyCityUppercase mary
  // Person 28 "Mary" (Address "83 Mary Ln" "Maryland" (Locality "MARY MARY" "Western Mary" "Maristan"))
  def modifyCityUppercase(person: Person): Person =
    sys.error("todo: modifyCityUppercase")
}
