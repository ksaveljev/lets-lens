package com.evolutiongaming.fp

import cats.arrow.Choice
import cats.kernel.Monoid
import cats.syntax.functor._
import cats.{Applicative, Foldable, Functor, Traverse}

object Lens {
  import Data._

  /*
   * Let's remind ourselves of Traversable, noting Foldable and Functor
   *
   * @typeclass trait Traverse[F[_]] extends Functor[F] with Foldable[F] { self =>
   *   /**
   *    * given a function which returns a G effect, thread this effect
   *    * through the running of this function on all the values in F,
   *    * returning an F[A] in a G context
   *    */
   *   def traverse[G[_]: Applicative, A, B](fa: F[A])(f: A => G[B]): G[F[B]]
   *
   *   /**
   *    * thread all the G effects through the F structure to invert the
   *    * structure from F[G[_]] to G[F[_]]
   *    */
   *   def sequence[G[_]: Applicative, A](fga: F[G[A]]): G[F[A]] =
   *     traverse(fga)(ga => ga)
   *   ....
   * }
   */

  // Observe that `fmap` can be recovered from `traverse` using `Identity`
  def fmapT[T[_]: Traverse, A, B](f: A => B)(ta: T[A]): T[B] =
    sys.error("todo: fmapT")

  // Let's refactor out the call to `traverse` as an argument to `fmapT`
  def over[S, T, A, B](t: (A => Identity[B]) => S => Identity[T])(f: A => B)(s: S): T =
    sys.error("todo: over")

  // Here is `fmapT` again, passing `traverse` to `over`
  def fmapTAgain[T[_], A, B](f: A => B)(ta: T[A])(implicit T: Traverse[T]): T[B] =
    sys.error("todo: fmapTAgain")


  // Let's create a type-alias for this type of function
  type Set[S, T, A, B] = (A => Identity[B]) => S => Identity[T]


  // Let's write an inverse to `over` that does the `Identity` wrapping & unwrapping
  def sets[S, T, A, B](t: (A => B) => S => T): Set[S, T, A, B] =
    sys.error("todo: sets")

  def mapped[F[_], A, B](implicit F: Functor[F]): Set[F[A], F[B], A, B] =
    sys.error("todo: mapped")

  def set[S, T, A, B](t: Set[S, T, A, B])(s: S)(b: B): T =
    sys.error("todo: set")


  // Observe that `foldMap` can be recovered from `traverse` using `Const`
  def foldMapT[T[_], A, B](f: A => B)(ta: T[A])(implicit T: Traverse[T], M: Monoid[B]) : B =
    sys.error("todo: foldMapT")

  // Let's refactor out the call to `traverse` as an argument to `foldMapT`
  def foldMapOf[S, T, R, A, B](t: (A => Const[R, B]) => S => Const[R, T])(f: A => R)(s: S): R =
    sys.error("todo: foldMapOf")

  // Here is `foldMapT` again, passing `traverse` to `foldMapOf`
  def foldMapTAgain[T[_], A, B](f: A => B)(ta: T[A])(implicit T: Traverse[T], M: Monoid[B]): B =
    sys.error("todo: foldMapTAgain")


  // Let's create a type-alias for this type of function
  trait Fold[S, T, A, B] {
    def f[R: Monoid](t: A => Const[R, B])(s: S): Const[R, T]
  }


  // Let's write an inverse to `foldMapOf` that does the `Const` wrapping & unwrapping
  def folds[S, T, A, B](t: (A => B) => S => T)(f: A => Const[B, A])(s: S): Const[T, S] =
    sys.error("todo: folds")

  def folded[F[_], A](implicit F: Foldable[F]): Fold[F[A], F[A], A, A] =
    sys.error("todo: folded")


  // `Get` is like `Fold` but without the `Monoid` constraint
  type Get[R, S, A] = (A => Const[R, A]) => S => Const[R, S]


  def get[S, A](g: Get[A, S, A])(s: S): A =
    sys.error("todo: get")


  // Let's generalise `Identity` and `Const r` to any `Applicative` instance
  trait Traversal[S, T, A, B] {
    def f[F[_]: Applicative](t: A => F[B])(s: S): F[T]
  }


  // Traverse both sides of a pair
  def both[A, B]: Traversal[(A, A), (B, B), A, B] =
    sys.error("todo: both")

  // Traverse the left side of `Either`
  def traverseLeft[A, B, X]: Traversal[Either[A, X], Either[B, X], A, B] =
    sys.error("todo: traverseLeft")

  // Traverse the right side of `Either`
  def traverseRight[A, B, X]: Traversal[Either[X, A], Either[X, B], A, B] =
    sys.error("todo: traverseRight")


  type SimpleTraversal[A, B] = Traversal[A, A, B, B]



  /*
   * `Const r` is `Applicative`, if `Monoid r`, however, without the `Monoid` constraint (as in `Get`)
   * the only shared abstraction between `Identity` and `Const r` is `Functor`
   *
   * Consequently, we arrive at our lens derivation:
   */
  trait Lens[S, T, A, B] {
    def f[F[_]: Functor](t: A => F[B])(s: S): F[T]
  }



  // A prism is a less specific type of traversal
  trait Prism[S, T, A, B] {
    def f[P[_, _]: Choice, F[_]: Applicative](p: P[A, F[B]]): P[S, F[T]]
  }

  def _left[A, B, X]: Prism[Either[A, X], Either[B, X], A, B] =
    sys.error("todo: _left")

  def _right[A, B, X]: Prism[Either[X, A], Either[X, B], A, B] =
    sys.error("todo: _right")

  def prism[S, T, A, B](f: B => T)(t: S => Either[T, A]): Prism[S, T, A, B] =
    sys.error("todo: prism")

  def _some[A, B]: Prism[Option[A], Option[B], A, B] =
    sys.error("todo: _just")

  def _none[A]: Prism[Option[A], Option[A], Unit, Unit] =
    sys.error("todo: _none")

  def setP[S, T, A, B](p: Prism[S, T, A, B])(s: S): Either[T, A] =
    sys.error("todo: setP")

  def getP[S, T, A, B](p: Prism[S, T, A, B])(b: B): T =
    sys.error("todo: getP")


  type SimplePrism[A, B] = Prism[A, A, B, B]

  //////////

  // |
  //
  // >>> modify fstL (+1) (0 :: Int, "abc")
  // (1,"abc")
  //
  // >>> modify sndL (+1) ("abc", 0 :: Int)
  // ("abc",1)
  //
  // prop> let types = (x :: Int, y :: String) in modify fstL id (x, y) == (x, y)
  //
  // prop> let types = (x :: Int, y :: String) in modify sndL id (x, y) == (x, y)
  def modify[S, T, A, B](lens: Lens[S, T, A, B])(f: A => B)(s: S): T =
    sys.error("todo: modify")

  // |
  //
  // >>> fstL .~ 1 $ (0 :: Int, "abc")
  // (1,"abc")
  //
  // >>> sndL .~ 1 $ ("abc", 0 :: Int)
  // ("abc",1)
  //
  // prop> let types = (x :: Int, y :: String) in set fstL (x, y) z == (fstL .~ z $ (x, y))
  //
  // prop> let types = (x :: Int, y :: String) in set sndL (x, y) z == (sndL .~ z $ (x, y))
  def `.~`[S, T, A, B](lens: Lens[S, T, A, B])(b: B)(s: S): T =
    sys.error("todo: `.~`")

  // |
  //
  // >>> fmodify fstL (+) (5 :: Int, "abc") 8
  // (13,"abc")
  //
  // >>> fmodify fstL (\n -> bool Nothing (Just (n * 2)) (even n)) (10, "abc")
  // Just (20,"abc")
  //
  // >>> fmodify fstL (\n -> bool Nothing (Just (n * 2)) (even n)) (11, "abc")
  // Nothing
  def fmodify[F[_]: Functor, S, T, A, B](lens: Lens[S, T, A, B])(f: A => F[B])(s: S): F[T] =
    sys.error("todo: fmodify")

  // |
  //
  // >>> fstL |= Just 3 $ (7, "abc")
  // Just (3,"abc")
  //
  // >>> (fstL |= (+1) $ (3, "abc")) 17
  // (18,"abc")
  def |=[F[_]: Functor, S, T, A, B](lens: Lens[S, T, A, B])(fb: F[B])(s: S): F[T] =
    sys.error("todo: |=")

  // |
  //
  // >>> modify fstL (*10) (3, "abc")
  // (30,"abc")
  def fstL[A, B, X]: Lens[(A, X), (B, X), A, B] =
    sys.error("todo: fstL")

  // |
  //
  // >>> modify sndL (++ "def") (13, "abc")
  // (13,"abcdef")
  def sndL[A, B, X]: Lens[(X, A), (X, B), A, B] =
    sys.error("todo: sndL")

  // |
  //
  // >>> get (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d']))
  // Just 'c'
  //
  // >>> get (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d']))
  // Nothing
  //
  // >>> set (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) (Just 'X')
  // fromList [(1,'a'),(2,'b'),(3,'X'),(4,'d')]
  //
  // >>> set (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) (Just 'X')
  // fromList [(1,'a'),(2,'b'),(3,'c'),(4,'d'),(33,'X')]
  //
  // >>> set (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) Nothing
  // fromList [(1,'a'),(2,'b'),(4,'d')]
  //
  // >>> set (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) Nothing
  // fromList [(1,'a'),(2,'b'),(3,'c'),(4,'d')]
  def mapL[K, V](key: K): Lens[Map[K, V], Map[K, V], Option[V], Option[V]] =
    sys.error("todo: mapL")

  // |
  //
  // >>> get (setL 3) (Set.fromList [1..5])
  // True
  //
  // >>> get (setL 33) (Set.fromList [1..5])
  // False
  //
  // >>> set (setL 3) (Set.fromList [1..5]) True
  // fromList [1,2,3,4,5]
  //
  // >>> set (setL 3) (Set.fromList [1..5]) False
  // fromList [1,2,4,5]
  //
  // >>> set (setL 33) (Set.fromList [1..5]) True
  // fromList [1,2,3,4,5,33]
  //
  // >>> set (setL 33) (Set.fromList [1..5]) False
  // fromList [1,2,3,4,5]
  def setL[K](key: K): Lens[Predef.Set[K], Predef.Set[K], Boolean, Boolean] =
    sys.error("todo: setL")

  // |
  //
  // >>> get (compose fstL sndL) ("abc", (7, "def"))
  // 7
  //
  // >>> set (compose fstL sndL) ("abc", (7, "def")) 8
  // ("abc",(8,"def"))
  def compose[S, T, A, B, Q, R](lens1: Lens[S, T, A, B])(lens2: Lens[Q, R, S, T]): Lens[Q, R, A, B] =
    sys.error("todo: compose")

  // |
  //
  // >>> get identity 3
  // 3
  //
  // >>> set identity 3 4
  // 4
  def identity[A, B]: Lens[A, B, A, B] =
    sys.error("todo: identity")

  // |
  //
  // >>> get (product fstL sndL) (("abc", 3), (4, "def"))
  // ("abc","def")
  //
  // >>> set (product fstL sndL) (("abc", 3), (4, "def")) ("ghi", "jkl")
  // (("ghi",3),(4,"jkl"))
  def product[S, T, A, B, Q, R, C, D](lens1: Lens[S, T, A, B])(lens2: Lens[Q, R, C, D]): Lens[(S, Q), (T, R), (A, C), (B, D)] =
    sys.error("todo: product")

  // |
  //
  // >>> get (choice fstL sndL) (Left ("abc", 7))
  // "abc"
  //
  // >>> get (choice fstL sndL) (Right ("abc", 7))
  // 7
  //
  // >>> set (choice fstL sndL) (Left ("abc", 7)) "def"
  // Left ("def",7)
  //
  // >>> set (choice fstL sndL) (Right ("abc", 7)) 8
  // Right ("abc",8)
  def choice[S, T, A, B, Q, R](lens1: Lens[S, T, A, B])(lens2: Lens[Q, R, A, B]): Lens[Either[S, Q], Either[T, R], A, B] =
    sys.error("todo: choice")

  //////////

  type SimpleLens[A, B] = Lens[A, A, B, B]


  val cityL: SimpleLens[Locality, String] = new SimpleLens[Locality, String] {
    def f[F[_] : Functor](t: String => F[String])(l: Locality): F[Locality] =
      t(l.city).map(city => Locality(city, l.state, l.country))
  }

  val stateL: SimpleLens[Locality, String] = new SimpleLens[Locality, String] {
    def f[F[_] : Functor](t: String => F[String])(l: Locality): F[Locality] =
      t(l.state).map(state => Locality(l.city, state, l.country))
  }

  val countryL: SimpleLens[Locality, String] = new SimpleLens[Locality, String] {
    def f[F[_] : Functor](t: String => F[String])(l: Locality): F[Locality] =
      t(l.country).map(country => Locality(l.city, l.state, country))
  }

  val streetL: SimpleLens[Address, String] = new SimpleLens[Address, String] {
    def f[F[_] : Functor](t: String => F[String])(a: Address): F[Address] =
      t(a.street).map(street => Address(street, a.suburb, a.locality))
  }

  val suburbL: SimpleLens[Address, String] = new SimpleLens[Address, String] {
    def f[F[_] : Functor](t: String => F[String])(a: Address): F[Address] =
      t(a.suburb).map(suburb => Address(a.street, suburb, a.locality))
  }

  val localityL: SimpleLens[Address, Locality] = new SimpleLens[Address, Locality] {
    def f[F[_] : Functor](t: Locality => F[Locality])(a: Address): F[Address] =
      t(a.locality).map(locality => Address(a.street, a.suburb, locality))
  }

  val ageL: SimpleLens[Person, Int] = new SimpleLens[Person, Int] {
    def f[F[_] : Functor](t: Int => F[Int])(p: Person): F[Person] =
      t(p.age).map(age => Person(age, p.name, p.address))
  }

  val nameL: SimpleLens[Person, String] = new SimpleLens[Person, String] {
    def f[F[_] : Functor](t: String => F[String])(p: Person): F[Person] =
      t(p.name).map(name => Person(p.age, name, p.address))
  }

  val addressL: SimpleLens[Person, Address] = new SimpleLens[Person, Address] {
    def f[F[_] : Functor](t: Address => F[Address])(p: Person): F[Person] =
      t(p.address).map(address => Person(p.age, p.name, address))
  }

  def intAndIntL[A]: SimpleLens[IntAnd[A], Int] = new SimpleLens[IntAnd[A], Int] {
    def f[F[_] : Functor](t: Int => F[Int])(s: IntAnd[A]): F[IntAnd[A]] =
      t(s.int).map(n => IntAnd(n, s.a))
  }

  def intAndL[A, B]: Lens[IntAnd[A], IntAnd[B], A, B] = new Lens[IntAnd[A], IntAnd[B], A, B] {
    def f[F[_] : Functor](t: A => F[B])(s: IntAnd[A]): F[IntAnd[B]] =
      t(s.a).map(a => IntAnd(s.int, a))
  }


  // |
  //
  // >>> getSuburb fred
  // "Fredville"
  //
  // >>> getSuburb mary
  // "Maryland"
  def getSuburb(person: Person): String =
    sys.error("todo: getSuburb")

  // |
  //
  // >>> setStreet fred "Some Other St"
  // Person 24 "Fred" (Address "Some Other St" "Fredville" (Locality "Fredmania" "New South Fred" "Fredalia"))
  //
  // >>> setStreet mary "Some Other St"
  // Person 28 "Mary" (Address "Some Other St" "Maryland" (Locality "Mary Mary" "Western Mary" "Maristan"))
  def setStreet(person: Person)(street: String): Person =
    sys.error("todo: setStreet")

  // |
  //
  // >>> getAgeAndCountry (fred, maryLocality)
  // (24,"Maristan")
  //
  // >>> getAgeAndCountry (mary, fredLocality)
  // (28,"Fredalia")
  def getAgeAndCountry(pl: (Person, Locality)): (Int, String) =
    sys.error("todo: getAgeAndCountry")

  // |
  //
  // >>> setCityAndLocality (fred, maryAddress) ("Some Other City", fredLocality)
  // (Person 24 "Fred" (Address "15 Fred St" "Fredville" (Locality "Some Other City" "New South Fred" "Fredalia")),Address "83 Mary Ln" "Maryland" (Locality "Fredmania" "New South Fred" "Fredalia"))
  //
  // >>> setCityAndLocality (mary, fredAddress) ("Some Other City", maryLocality)
  // (Person 28 "Mary" (Address "83 Mary Ln" "Maryland" (Locality "Some Other City" "Western Mary" "Maristan")),Address "15 Fred St" "Fredville" (Locality "Mary Mary" "Western Mary" "Maristan"))
  def setCityAndLocality(pa: (Person, Address))(cl: (String, Locality)): (Person, Address) =
    sys.error("todo: setCityAndLocality")

  // |
  //
  // >>> getSuburbOrCity (Left maryAddress)
  // "Maryland"
  //
  // >>> getSuburbOrCity (Right fredLocality)
  // "Fredmania"
  def getSuburbOrCity(al: Either[Address, Locality]): String =
    sys.error("todo: getSuburbOrCity")

  // |
  //
  // >>> setStreetOrState (Right maryLocality) "Some Other State"
  // Right (Locality "Mary Mary" "Some Other State" "Maristan")
  //
  // >>> setStreetOrState (Left fred) "Some Other St"
  // Left (Person 24 "Fred" (Address "Some Other St" "Fredville" (Locality "Fredmania" "New South Fred" "Fredalia")))
  def setStreetOrState(pl: Either[Person, Locality])(s: String): Either[Person, Locality] =
    sys.error("todo: setStreetOrState")

  // |
  //
  // >>> modifyCityUppercase fred
  // Person 24 "Fred" (Address "15 Fred St" "Fredville" (Locality "FREDMANIA" "New South Fred" "Fredalia"))
  //
  // >>> modifyCityUppercase mary
  // Person 28 "Mary" (Address "83 Mary Ln" "Maryland" (Locality "MARY MARY" "Western Mary" "Maristan"))
  def modifyCityUppercase(person: Person): Person =
    sys.error("todo: modifyCityUppercase")

  // |
  //
  // >>> modifyIntAndLengthEven (IntAnd 10 "abc")
  // IntAnd 10 False
  //
  // >>> modifyIntAndLengthEven (IntAnd 10 "abcd")
  // IntAnd 10 True
  def modifyIntAndLengthEven[A](ia: IntAnd[List[A]]): IntAnd[Boolean] =
    sys.error("todo: modifyIntAndLengthEven")

  //////////

  // |
  //
  // >>> over traverseLocality (map toUpper) (Locality "abc" "def" "ghi")
  // Locality "ABC" "DEF" "GHI"
  def traverseLocality: SimpleTraversal[Locality, String] =
    sys.error("todo: traverseLocality")

  // |
  //
  // >>> over intOrIntP (*10) (IntOrIs 3)
  // IntOrIs 30
  //
  // >>> over intOrIntP (*10) (IntOrIsNot "abc")
  // IntOrIsNot "abc"
  def intOrIntP[A]: SimplePrism[IntOr[A], Int] =
    sys.error("todo: intOrIntP")

  def intOrP[A, B]: Prism[IntOr[A], IntOr[B], A, B] =
    sys.error("todo: intOrP")

  // |
  //
  // >> intOrLengthEven (IntOrIsNot "abc")
  // IntOrIsNot False
  //
  // >>> intOrLengthEven (IntOrIsNot "abcd")
  // IntOrIsNot True
  //
  // >>> intOrLengthEven (IntOrIs 10)
  // IntOrIs 10
  def intOrLengthEven[A](is: IntOr[List[A]]): IntOr[Boolean] =
    sys.error("todo: intOrLengthEven")
}
