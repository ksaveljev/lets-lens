package com.evolutiongaming.fp

import cats.Functor
import cats.syntax.functor._
import com.evolutiongaming.fp.Data._

object OpticPolyLens {

  trait Lens[S, T, A, B] {
    def modify[F[_]: Functor](f: A => F[B])(s: S): F[T]
  }

  def get[S, T, A, B](lens: Lens[S, T, A, B])(s: S): A =
    lens.modify[Const[A, ?]](x => Const(x))(s).getConst

  def set[S, T, A, B](lens: Lens[S, T, A, B])(s: S)(b: B): T =
    lens.modify[Identity](_ => Identity(b))(s).getIdentity


  // The get/set law of lenses. This function should always return true.
  def getsetLaw[S, A](lens: Lens[S, S, A, A])(s: S): Boolean =
    set(lens)(s)(get(lens)(s)) == s

  // The set/get law of lenses. This function should always return true.
  def setgetLaw[S, A](lens: Lens[S, S, A, A])(s: S)(a: A): Boolean =
    get(lens)(set(lens)(s)(a)) == a

  // The set/set law of lenses. This function should always return true.
  def setsetLaw[S, A, B](lens: Lens[S, S, A, B])(s: S)(b1: B)(b2: B): Boolean =
    set(lens)(set(lens)(s)(b1))(b2) == set(lens)(s)(b2)

  //////////

  // |
  //
  // >>> modify fstL (+1) (0 :: Int, "abc")
  // (1,"abc")
  //
  // >>> modify sndL (+1) ("abc", 0 :: Int)
  // ("abc",1)
  //
  // prop> let types = (x :: Int, y :: String) in modify fstL id (x, y) == (x, y)
  //
  // prop> let types = (x :: Int, y :: String) in modify sndL id (x, y) == (x, y)
  def modify[S, T, A, B](lens: Lens[S, T, A, B])(f: A => B)(s: S): T =
    sys.error("todo: modify")

  // |
  //
  // >>> fstL .~ 1 $ (0 :: Int, "abc")
  // (1,"abc")
  //
  // >>> sndL .~ 1 $ ("abc", 0 :: Int)
  // ("abc",1)
  //
  // prop> let types = (x :: Int, y :: String) in set fstL (x, y) z == (fstL .~ z $ (x, y))
  //
  // prop> let types = (x :: Int, y :: String) in set sndL (x, y) z == (sndL .~ z $ (x, y))
  def `.~`[S, T, A, B](lens: Lens[S, T, A, B])(b: B)(s: S): T =
    sys.error("todo: `.~`")

  // |
  //
  // >>> fmodify fstL (+) (5 :: Int, "abc") 8
  // (13,"abc")
  //
  // >>> fmodify fstL (\n -> bool Nothing (Just (n * 2)) (even n)) (10, "abc")
  // Just (20,"abc")
  //
  // >>> fmodify fstL (\n -> bool Nothing (Just (n * 2)) (even n)) (11, "abc")
  // Nothing
  def fmodify[F[_]: Functor, S, T, A, B](lens: Lens[S, T, A, B])(f: A => F[B])(s: S): F[T] =
    sys.error("todo: fmodify")

  // |
  //
  // >>> fstL |= Just 3 $ (7, "abc")
  // Just (3,"abc")
  //
  // >>> (fstL |= (+1) $ (3, "abc")) 17
  // (18,"abc")
  def |=[F[_]: Functor, S, T, A, B](lens: Lens[S, T, A, B])(fb: F[B])(s: S): F[T] =
    sys.error("todo: |=")

  // |
  //
  // >>> modify fstL (*10) (3, "abc")
  // (30,"abc")
  //
  // prop> let types = (x :: Int, y :: String) in getsetLaw fstL (x, y)
  //
  // prop> let types = (x :: Int, y :: String) in setgetLaw fstL (x, y) z
  //
  // prop> let types = (x :: Int, y :: String) in setsetLaw fstL (x, y) z
  def fstL[A, B, X]: Lens[(A, X), (B, X), A, B] =
    sys.error("todo: fstL")

  // |
  //
  // >>> modify sndL (++ "def") (13, "abc")
  // (13,"abcdef")
  //
  // prop> let types = (x :: Int, y :: String) in getsetLaw sndL (x, y)
  //
  // prop> let types = (x :: Int, y :: String) in setgetLaw sndL (x, y) z
  //
  // prop> let types = (x :: Int, y :: String) in setsetLaw sndL (x, y) z
  def sndL[A, B, X]: Lens[(X, A), (X, B), A, B] =
    sys.error("todo: sndL")

  // |
  //
  // >>> get (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d']))
  // Just 'c'
  //
  // >>> get (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d']))
  // Nothing
  //
  // >>> set (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) (Just 'X')
  // fromList [(1,'a'),(2,'b'),(3,'X'),(4,'d')]
  //
  // >>> set (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) (Just 'X')
  // fromList [(1,'a'),(2,'b'),(3,'c'),(4,'d'),(33,'X')]
  //
  // >>> set (mapL 3) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) Nothing
  // fromList [(1,'a'),(2,'b'),(4,'d')]
  //
  // >>> set (mapL 33) (Map.fromList (map (\c -> (ord c - 96, c)) ['a'..'d'])) Nothing
  // fromList [(1,'a'),(2,'b'),(3,'c'),(4,'d')]
  def mapL[K, V](key: K): Lens[Map[K, V], Map[K, V], Option[V], Option[V]] =
    sys.error("todo: mapL")

  // |
  //
  // >>> get (setL 3) (Set.fromList [1..5])
  // True
  //
  // >>> get (setL 33) (Set.fromList [1..5])
  // False
  //
  // >>> set (setL 3) (Set.fromList [1..5]) True
  // fromList [1,2,3,4,5]
  //
  // >>> set (setL 3) (Set.fromList [1..5]) False
  // fromList [1,2,4,5]
  //
  // >>> set (setL 33) (Set.fromList [1..5]) True
  // fromList [1,2,3,4,5,33]
  //
  // >>> set (setL 33) (Set.fromList [1..5]) False
  // fromList [1,2,3,4,5]
  def setL[K](key: K): Lens[Set[K], Set[K], Boolean, Boolean] =
    sys.error("todo: setL")

  // |
  //
  // >>> get (compose fstL sndL) ("abc", (7, "def"))
  // 7
  //
  // >>> set (compose fstL sndL) ("abc", (7, "def")) 8
  // ("abc",(8,"def"))
  def compose[S, T, A, B, Q, R](lens1: Lens[S, T, A, B])(lens2: Lens[Q, R, S, T]): Lens[Q, R, A, B] =
    sys.error("todo: compose")

  // |
  //
  // >>> get identity 3
  // 3
  //
  // >>> set identity 3 4
  // 4
  def identity[A, B]: Lens[A, B, A, B] =
    sys.error("todo: identity")

  // |
  //
  // >>> get (product fstL sndL) (("abc", 3), (4, "def"))
  // ("abc","def")
  //
  // >>> set (product fstL sndL) (("abc", 3), (4, "def")) ("ghi", "jkl")
  // (("ghi",3),(4,"jkl"))
  def product[S, T, A, B, Q, R, C, D](lens1: Lens[S, T, A, B])(lens2: Lens[Q, R, C, D]): Lens[(S, Q), (T, R), (A, C), (B, D)] =
    sys.error("todo: product")

  // |
  //
  // >>> get (choice fstL sndL) (Left ("abc", 7))
  // "abc"
  //
  // >>> get (choice fstL sndL) (Right ("abc", 7))
  // 7
  //
  // >>> set (choice fstL sndL) (Left ("abc", 7)) "def"
  // Left ("def",7)
  //
  // >>> set (choice fstL sndL) (Right ("abc", 7)) 8
  // Right ("abc",8)
  def choice[S, T, A, B, Q, R](lens1: Lens[S, T, A, B])(lens2: Lens[Q, R, A, B]): Lens[Either[S, Q], Either[T, R], A, B] =
    sys.error("todo: choice")

  //////////

  type SimpleLens[A, B] = Lens[A, A, B, B]


  val cityL: SimpleLens[Locality, String] = new SimpleLens[Locality, String] {
    def modify[F[_] : Functor](f: String => F[String])(l: Locality): F[Locality] =
      f(l.city).map(city => Locality(city, l.state, l.country))
  }

  val stateL: SimpleLens[Locality, String] = new SimpleLens[Locality, String] {
    def modify[F[_] : Functor](f: String => F[String])(l: Locality): F[Locality] =
      f(l.state).map(state => Locality(l.city, state, l.country))
  }

  val countryL: SimpleLens[Locality, String] = new SimpleLens[Locality, String] {
    def modify[F[_] : Functor](f: String => F[String])(l: Locality): F[Locality] =
      f(l.country).map(country => Locality(l.city, l.state, country))
  }

  val streetL: SimpleLens[Address, String] = new SimpleLens[Address, String] {
    def modify[F[_] : Functor](f: String => F[String])(a: Address): F[Address] =
      f(a.street).map(street => Address(street, a.suburb, a.locality))
  }

  val suburbL: SimpleLens[Address, String] = new SimpleLens[Address, String] {
    def modify[F[_] : Functor](f: String => F[String])(a: Address): F[Address] =
      f(a.suburb).map(suburb => Address(a.street, suburb, a.locality))
  }

  val localityL: SimpleLens[Address, Locality] = new SimpleLens[Address, Locality] {
    def modify[F[_] : Functor](f: Locality => F[Locality])(a: Address): F[Address] =
      f(a.locality).map(locality => Address(a.street, a.suburb, locality))
  }

  val ageL: SimpleLens[Person, Int] = new SimpleLens[Person, Int] {
    def modify[F[_] : Functor](f: Int => F[Int])(p: Person): F[Person] =
      f(p.age).map(age => Person(age, p.name, p.address))
  }

  val nameL: SimpleLens[Person, String] = new SimpleLens[Person, String] {
    def modify[F[_] : Functor](f: String => F[String])(p: Person): F[Person] =
      f(p.name).map(name => Person(p.age, name, p.address))
  }

  val addressL: SimpleLens[Person, Address] = new SimpleLens[Person, Address] {
    def modify[F[_] : Functor](f: Address => F[Address])(p: Person): F[Person] =
      f(p.address).map(address => Person(p.age, p.name, address))
  }

  def intAndIntL[A]: SimpleLens[IntAnd[A], Int] = new SimpleLens[IntAnd[A], Int] {
    def modify[F[_] : Functor](f: Int => F[Int])(s: IntAnd[A]): F[IntAnd[A]] =
      f(s.int).map(n => IntAnd(n, s.a))
  }

  def intAndL[A, B]: Lens[IntAnd[A], IntAnd[B], A, B] = new Lens[IntAnd[A], IntAnd[B], A, B] {
    def modify[F[_] : Functor](f: A => F[B])(s: IntAnd[A]): F[IntAnd[B]] =
      f(s.a).map(b => IntAnd(s.int, b))
  }


  // |
  //
  // >>> getSuburb fred
  // "Fredville"
  //
  // >>> getSuburb mary
  // "Maryland"
  def getSuburb(person: Person): String =
    sys.error("todo: getSuburb")

  // |
  //
  // >>> setStreet fred "Some Other St"
  // Person 24 "Fred" (Address "Some Other St" "Fredville" (Locality "Fredmania" "New South Fred" "Fredalia"))
  //
  // >>> setStreet mary "Some Other St"
  // Person 28 "Mary" (Address "Some Other St" "Maryland" (Locality "Mary Mary" "Western Mary" "Maristan"))
  def setStreet(person: Person)(street: String): Person =
    sys.error("todo: setStreet")

  // |
  //
  // >>> getAgeAndCountry (fred, maryLocality)
  // (24,"Maristan")
  //
  // >>> getAgeAndCountry (mary, fredLocality)
  // (28,"Fredalia")
  def getAgeAndCountry(pl: (Person, Locality)): (Int, String) =
    sys.error("todo: getAgeAndCountry")

  // |
  //
  // >>> setCityAndLocality (fred, maryAddress) ("Some Other City", fredLocality)
  // (Person 24 "Fred" (Address "15 Fred St" "Fredville" (Locality "Some Other City" "New South Fred" "Fredalia")),Address "83 Mary Ln" "Maryland" (Locality "Fredmania" "New South Fred" "Fredalia"))
  //
  // >>> setCityAndLocality (mary, fredAddress) ("Some Other City", maryLocality)
  // (Person 28 "Mary" (Address "83 Mary Ln" "Maryland" (Locality "Some Other City" "Western Mary" "Maristan")),Address "15 Fred St" "Fredville" (Locality "Mary Mary" "Western Mary" "Maristan"))
  def setCityAndLocality(pa: (Person, Address))(cl: (String, Locality)): (Person, Address) =
    sys.error("todo: setCityAndLocality")

  // |
  //
  // >>> getSuburbOrCity (Left maryAddress)
  // "Maryland"
  //
  // >>> getSuburbOrCity (Right fredLocality)
  // "Fredmania"
  def getSuburbOrCity(al: Either[Address, Locality]): String =
    sys.error("todo: getSuburbOrCity")

  // |
  //
  // >>> setStreetOrState (Right maryLocality) "Some Other State"
  // Right (Locality "Mary Mary" "Some Other State" "Maristan")
  //
  // >>> setStreetOrState (Left fred) "Some Other St"
  // Left (Person 24 "Fred" (Address "Some Other St" "Fredville" (Locality "Fredmania" "New South Fred" "Fredalia")))
  def setStreetOrState(pl: Either[Person, Locality])(s: String): Either[Person, Locality] =
    sys.error("todo: setStreetOrState")

  // |
  //
  // >>> modifyCityUppercase fred
  // Person 24 "Fred" (Address "15 Fred St" "Fredville" (Locality "FREDMANIA" "New South Fred" "Fredalia"))
  //
  // >>> modifyCityUppercase mary
  // Person 28 "Mary" (Address "83 Mary Ln" "Maryland" (Locality "MARY MARY" "Western Mary" "Maristan"))
  def modifyCityUppercase(person: Person): Person =
    sys.error("todo: modifyCityUppercase")

  // |
  //
  // >>> modify intAndL (even . length) (IntAnd 10 "abc")
  // IntAnd 10 False
  //
  // >>> modify intAndL (even . length) (IntAnd 10 "abcd")
  // IntAnd 10 True
  def modifyIntAndLengthEven[A](ia: IntAnd[List[A]]): IntAnd[Boolean] =
    sys.error("todo: modifyIntAndLengthEvent")
}
