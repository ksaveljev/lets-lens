package com.evolutiongaming.fp

import org.scalatest.FreeSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import cats.instances.function._
import cats.instances.option._

class OpticPolyLensSpec extends FreeSpec with GeneratorDrivenPropertyChecks {
  import OpticPolyLens._
  import Data._

  "OpticPolyLens" -{
    "modify" in {
      assert(modify(fstL[Int, Int, String])(_ + 1)(0, "abc") == (1, "abc"))
      assert(modify(sndL[String, String , Int])(_ + "d")(0, "abc") == (0, "abcd"))
      forAll { (input: (Int, String)) =>
        val (x, y) = input
        assert(modify(fstL[Int, Int, String])(Predef.identity[Int])(input) == input)
        assert(modify(sndL[String, String, Int])(Predef.identity[String])(input) == input)
      }
    }

    ".~" in {
      assert(`.~`(fstL[Int, Int, String])(1)(0, "abc") == (1, "abc"))
      assert(`.~`(sndL[String, String, Int])("hi")(0, "abc") == (0, "hi"))
      forAll { (input: (Int, String, Int, String)) =>
        val (x, y, xx, yy) = input
        assert(set(fstL[Int, Int, String])(x, y)(xx) == `.~`(fstL[Int, Int, String])(xx)(x, y))
        assert(set(sndL[String, String, Int])(x, y)(yy) == `.~`(sndL[String, String, Int])(yy)(x, y))
      }
    }

    "fmodify" in {
      assert(fmodify[Function1[Int, ?], (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])((a: Int) => (b: Int) => a + b)(5, "abc").apply(8) == (13, "abc"))
      assert(fmodify[Option, (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])((n: Int) => Data.bool[Option[Int]](None)(Some(n * 2))(n % 2 == 0))(10, "abc") == Some(20, "abc"))
      assert(fmodify[Option, (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])((n: Int) => Data.bool[Option[Int]](None)(Some(n * 2))(n % 2 == 0))(11, "abc") == None)
    }

    "|=" in {
      assert(|=[Option, (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])(Some(3))(7, "abc") == Some(3, "abc"))
      assert(|=[Function1[Int, ?], (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])(_ + 1)(3, "abc").apply(17) == (18, "abc"))
    }

    "fstL" in {
      assert(modify(fstL[Int, Int, String])(_ * 10)(3, "abc") == (30, "abc"))
      forAll { (input: (Int, String, Int, Int)) =>
        val (x, y, xx, xxx) = input
        assert(getsetLaw(fstL[Int, Int, String])(x, y))
        assert(setgetLaw(fstL[Int, Int, String])(x, y)(xx))
        assert(setsetLaw(fstL[Int, Int, String])(x, y)(xx)(xxx))
      }
    }

    "sndL" in {
      assert(modify(sndL[String, String, Int])(_ + "def")(13, "abc") == (13, "abcdef"))
      forAll { (input: (Int, String, String, String)) =>
        val (x, y, xx, xxx) = input
        assert(getsetLaw(sndL[String, String, Int])(x, y))
        assert(setgetLaw(sndL[String, String, Int])(x, y)(xx))
        assert(setsetLaw(sndL[String, String, Int])(x, y)(xx)(xxx))
      }
    }

    "mapL" in {
      val m: Map[Int, Char] = List((1, 'a'), (2, 'b'), (3, 'c'), (4, 'd')).toMap
      assert(get(mapL[Int, Char](3))(m) == Some('c'))
      assert(get(mapL[Int, Char](33))(m) == None)
      assert(set(mapL[Int, Char](3))(m)(Some('X')) == List((1, 'a'), (2, 'b'), (3, 'X'), (4, 'd')).toMap)
      assert(set(mapL[Int, Char](33))(m)(Some('X')) == List((1, 'a'), (2, 'b'), (3, 'c'), (4, 'd'), (33, 'X')).toMap)
      assert(set(mapL[Int, Char](3))(m)(None) == List((1, 'a'), (2, 'b'), (4, 'd')).toMap)
      assert(set(mapL[Int, Char](33))(m)(None) == m)
    }

    "setL" in {
      val s = Set(1, 2, 3, 4, 5)
      assert(get(setL(3))(s) == true)
      assert(get(setL(33))(s) == false)
      assert(set(setL(3))(s)(true) == s)
      assert(set(setL(3))(s)(false) == Set(1, 2, 4, 5))
      assert(set(setL(33))(s)(true) == Set(1, 2, 3, 4, 5, 33))
      assert(set(setL(33))(s)(false) == s)
    }

    "compose" in {
      assert(get(compose(fstL[Int, Int, String])(sndL[(Int, String), (Int, String), String]))("abc", (7, "def")) == 7)
      assert(set(compose(fstL[Int, Int, String])(sndL[(Int, String), (Int, String), String]))("abc", (7, "def"))(8) == ("abc", (8, "def")))
    }

    "identity" in {
      assert(get(identity[Int, Int])(3) == 3)
      assert(set(identity[Int, Int])(3)(4) == 4)
    }

    "product" in {
      assert(get(product(fstL[String, String, Int])(sndL[String, String, Int]))(("abc", 3), (4, "def")) == ("abc", "def"))
      assert(set(product(fstL[String, String, Int])(sndL[String, String, Int]))(("abc", 3), (4, "def"))("ghi", "jkl") == (("ghi", 3), (4, "jkl")))
    }

    "choice" in {
      assert(get(choice(fstL[String, String, Int])(sndL[String, String, Int]))(Left[(String, Int), (Int, String)]("abc", 7)) == "abc")
      assert(get(choice(fstL[Int, Int, String])(sndL[Int, Int, String]))(Right[(Int, String), (String, Int)]("abc", 7)) == 7)
      assert(set(choice(fstL[String, String, Int])(sndL[String, String, Int]))(Left[(String, Int), (Int, String)]("abc", 7))("def") == Left("def", 7))
      assert(set(choice(fstL[Int, Int, String])(sndL[Int, Int, String]))(Right[(Int, String), (String, Int)]("abc", 7))(8) == Right("abc", 8))
    }

    "getSuburb" in {
      assert(getSuburb(fred) == "Fredville")
      assert(getSuburb(mary) == "Maryland")
    }

    "setStreet" in {
      assert(setStreet(fred)("Some Other St") == Person(24, "Fred", Address("Some Other St", "Fredville", Locality("Fredmania", "New South Fred", "Fredalia"))))
      assert(setStreet(mary)("Some Other St") == Person(28, "Mary", Address("Some Other St", "Maryland", Locality("Mary Mary", "Western Mary", "Maristan"))))
    }

    "getAgeAndCountry" in {
      assert(getAgeAndCountry(fred, maryLocality) == (24, "Maristan"))
      assert(getAgeAndCountry(mary, fredLocality) == (28, "Fredalia"))
    }

    "setCityAndLocality" in {
      assert(setCityAndLocality(fred, maryAddress)("Some Other City", fredLocality) == (Person(24, "Fred", Address("15 Fred St", "Fredville", Locality("Some Other City", "New South Fred", "Fredalia"))), Address("83 Mary Ln", "Maryland", Locality("Fredmania", "New South Fred", "Fredalia"))))
      assert(setCityAndLocality(mary, fredAddress)("Some Other City", maryLocality) == (Person(28, "Mary", Address("83 Mary Ln", "Maryland", Locality("Some Other City", "Western Mary", "Maristan"))), Address("15 Fred St", "Fredville", Locality("Mary Mary", "Western Mary", "Maristan"))))
    }

    "getSuburbOrCity" in {
      assert(getSuburbOrCity(Left(maryAddress)) == "Maryland")
      assert(getSuburbOrCity(Right(fredLocality)) == "Fredmania")
    }

    "setStreetOrState" in {
      assert(setStreetOrState(Right(maryLocality))("Some Other State") == Right(Locality("Mary Mary", "Some Other State", "Maristan")))
      assert(setStreetOrState(Left(fred))("Some Other St") == Left(Person(24, "Fred", Address("Some Other St", "Fredville", Locality("Fredmania", "New South Fred", "Fredalia")))))
    }

    "modifyCityUppercase" in {
      assert(modifyCityUppercase(fred) == Person(24, "Fred", Address("15 Fred St", "Fredville", Locality("FREDMANIA", "New South Fred", "Fredalia"))))
      assert(modifyCityUppercase(mary) == Person(28, "Mary", Address("83 Mary Ln", "Maryland", Locality("MARY MARY", "Western Mary", "Maristan"))))
    }

    "modifyIntAndLengthEven" in {
      assert(modifyIntAndLengthEven(IntAnd[List[Char]](10, "abc".toList)) == IntAnd(10, false))
      assert(modifyIntAndLengthEven(IntAnd[List[Char]](10, "abce".toList)) == IntAnd(10, true))
    }
  }
}
