package com.evolutiongaming.fp

import org.scalatest.FreeSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import cats.instances.function._
import cats.instances.option._

class LensSpec extends FreeSpec with GeneratorDrivenPropertyChecks {
  import Lens._
  import Data._

  "Lens" -{
    "modify" in {
      assert(modify(fstL[Int, Int, String])(_ + 1)(0, "abc") == (1, "abc"))
      assert(modify(sndL[String, String , Int])(_ + "d")(0, "abc") == (0, "abcd"))
      forAll { (input: (Int, String)) =>
        val (x, y) = input
        assert(modify(fstL[Int, Int, String])(Predef.identity[Int])(input) == input)
        assert(modify(sndL[String, String, Int])(Predef.identity[String])(input) == input)
      }
    }

    ".~" in {
      assert(`.~`(fstL[Int, Int, String])(1)(0, "abc") == (1, "abc"))
      assert(`.~`(sndL[String, String, Int])("hi")(0, "abc") == (0, "hi"))
      forAll { (input: (Int, String, Int, String)) =>
        val (x, y, xx, yy) = input
        assert(set[(Int, String), (Int, String), Int, Int](fstL[Int, Int, String].f[Identity])(x, y)(xx) == `.~`(fstL[Int, Int, String])(xx)(x, y))
        assert(set[(Int, String), (Int, String), String, String](sndL[String, String, Int].f[Identity])(x, y)(yy) == `.~`(sndL[String, String, Int])(yy)(x, y))
      }
    }

    "fmodify" in {
      assert(fmodify[Function1[Int, ?], (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])((a: Int) => (b: Int) => a + b)(5, "abc").apply(8) == (13, "abc"))
      assert(fmodify[Option, (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])((n: Int) => Data.bool[Option[Int]](None)(Some(n * 2))(n % 2 == 0))(10, "abc") == Some(20, "abc"))
      assert(fmodify[Option, (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])((n: Int) => Data.bool[Option[Int]](None)(Some(n * 2))(n % 2 == 0))(11, "abc") == None)
    }

    "|=" in {
      assert(|=[Option, (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])(Some(3))(7, "abc") == Some(3, "abc"))
      assert(|=[Function1[Int, ?], (Int, String), (Int, String), Int, Int](fstL[Int, Int, String])(_ + 1)(3, "abc").apply(17) == (18, "abc"))
    }

    "fstL" in {
      assert(modify(fstL[Int, Int, String])(_ * 10)(3, "abc") == (30, "abc"))
    }

    "sndL" in {
      assert(modify(sndL[String, String, Int])(_ + "def")(13, "abc") == (13, "abcdef"))
    }

    "mapL" in {
      val m: Map[Int, Char] = List((1, 'a'), (2, 'b'), (3, 'c'), (4, 'd')).toMap
      assert(get(mapL[Int, Char](3).f[Const[Option[Char], ?]])(m) == Some('c'))
      assert(get(mapL[Int, Char](33).f[Const[Option[Char], ?]])(m) == None)
      assert(set[Map[Int, Char], Map[Int, Char], Option[Char], Option[Char]](mapL[Int, Char](3).f[Identity])(m)(Some('X')) == List((1, 'a'), (2, 'b'), (3, 'X'), (4, 'd')).toMap)
      assert(set[Map[Int, Char], Map[Int, Char], Option[Char], Option[Char]](mapL[Int, Char](33).f[Identity])(m)(Some('X')) == List((1, 'a'), (2, 'b'), (3, 'c'), (4, 'd'), (33, 'X')).toMap)
      assert(set[Map[Int, Char], Map[Int, Char], Option[Char], Option[Char]](mapL[Int, Char](3).f[Identity])(m)(None) == List((1, 'a'), (2, 'b'), (4, 'd')).toMap)
      assert(set[Map[Int, Char], Map[Int, Char], Option[Char], Option[Char]](mapL[Int, Char](33).f[Identity])(m)(None) == m)
    }

    "setL" in {
      val s = Set(1, 2, 3, 4, 5)
      assert(get(setL(3).f[Const[Boolean, ?]])(s) == true)
      assert(get(setL(33).f[Const[Boolean, ?]])(s) == false)
      assert(set[Predef.Set[Int], Predef.Set[Int], Boolean, Boolean](setL(3).f[Identity])(s)(true) == s)
      assert(set[Predef.Set[Int], Predef.Set[Int], Boolean, Boolean](setL(3).f[Identity])(s)(false) == Set(1, 2, 4, 5))
      assert(set[Predef.Set[Int], Predef.Set[Int], Boolean, Boolean](setL(33).f[Identity])(s)(true) == Set(1, 2, 3, 4, 5, 33))
      assert(set[Predef.Set[Int], Predef.Set[Int], Boolean, Boolean](setL(33).f[Identity])(s)(false) == s)
    }

    "compose" in {
      assert(get(compose(fstL[Int, Int, String])(sndL[(Int, String), (Int, String), String]).f[Const[Int, ?]])("abc", (7, "def")) == 7)
      assert(set[(String, (Int, String)), (String, (Int, String)), Int, Int](compose(fstL[Int, Int, String])(sndL[(Int, String), (Int, String), String]).f[Identity])("abc", (7, "def"))(8) == ("abc", (8, "def")))
    }

    "identity" in {
      assert(get(identity[Int, Int].f[Const[Int, ?]])(3) == 3)
      assert(set[Int, Int, Int, Int](identity[Int, Int].f[Identity])(3)(4) == 4)
    }

    "product" in {
      assert(get(product(fstL[String, String, Int])(sndL[String, String, Int]).f[Const[(String, String), ?]])(("abc", 3), (4, "def")) == ("abc", "def"))
      assert(set[((String, Int), (Int, String)), ((String, Int), (Int, String)), (String, String), (String, String)](product(fstL[String, String, Int])(sndL[String, String, Int]).f[Identity])(("abc", 3), (4, "def"))("ghi", "jkl") == (("ghi", 3), (4, "jkl")))
    }

    "choice" in {
      assert(get(choice(fstL[String, String, Int])(sndL[String, String, Int]).f[Const[String, ?]])(Left[(String, Int), (Int, String)]("abc", 7)) == "abc")
      assert(get(choice(fstL[Int, Int, String])(sndL[Int, Int, String]).f[Const[Int, ?]])(Right[(Int, String), (String, Int)]("abc", 7)) == 7)
      assert(set[Either[(String, Int), (Int, String)], Either[(String, Int), (Int, String)], String, String](choice(fstL[String, String, Int])(sndL[String, String, Int]).f[Identity])(Left[(String, Int), (Int, String)]("abc", 7))("def") == Left("def", 7))
      assert(set[Either[(Int, String), (String, Int)], Either[(Int, String), (String, Int)], Int, Int](choice(fstL[Int, Int, String])(sndL[Int, Int, String]).f[Identity])(Right[(Int, String), (String, Int)]("abc", 7))(8) == Right("abc", 8))
    }

    "getSuburb" in {
      assert(getSuburb(fred) == "Fredville")
      assert(getSuburb(mary) == "Maryland")
    }

    "setStreet" in {
      assert(setStreet(fred)("Some Other St") == Person(24, "Fred", Address("Some Other St", "Fredville", Locality("Fredmania", "New South Fred", "Fredalia"))))
      assert(setStreet(mary)("Some Other St") == Person(28, "Mary", Address("Some Other St", "Maryland", Locality("Mary Mary", "Western Mary", "Maristan"))))
    }

    "getAgeAndCountry" in {
      assert(getAgeAndCountry(fred, maryLocality) == (24, "Maristan"))
      assert(getAgeAndCountry(mary, fredLocality) == (28, "Fredalia"))
    }

    "setCityAndLocality" in {
      assert(setCityAndLocality(fred, maryAddress)("Some Other City", fredLocality) == (Person(24, "Fred", Address("15 Fred St", "Fredville", Locality("Some Other City", "New South Fred", "Fredalia"))), Address("83 Mary Ln", "Maryland", Locality("Fredmania", "New South Fred", "Fredalia"))))
      assert(setCityAndLocality(mary, fredAddress)("Some Other City", maryLocality) == (Person(28, "Mary", Address("83 Mary Ln", "Maryland", Locality("Some Other City", "Western Mary", "Maristan"))), Address("15 Fred St", "Fredville", Locality("Mary Mary", "Western Mary", "Maristan"))))
    }

    "getSuburbOrCity" in {
      assert(getSuburbOrCity(Left(maryAddress)) == "Maryland")
      assert(getSuburbOrCity(Right(fredLocality)) == "Fredmania")
    }

    "setStreetOrState" in {
      assert(setStreetOrState(Right(maryLocality))("Some Other State") == Right(Locality("Mary Mary", "Some Other State", "Maristan")))
      assert(setStreetOrState(Left(fred))("Some Other St") == Left(Person(24, "Fred", Address("Some Other St", "Fredville", Locality("Fredmania", "New South Fred", "Fredalia")))))
    }

    "modifyCityUppercase" in {
      assert(modifyCityUppercase(fred) == Person(24, "Fred", Address("15 Fred St", "Fredville", Locality("FREDMANIA", "New South Fred", "Fredalia"))))
      assert(modifyCityUppercase(mary) == Person(28, "Mary", Address("83 Mary Ln", "Maryland", Locality("MARY MARY", "Western Mary", "Maristan"))))
    }

    "modifyIntAndLengthEven" in {
      assert(modifyIntAndLengthEven(IntAnd[List[Char]](10, "abc".toList)) == IntAnd(10, false))
      assert(modifyIntAndLengthEven(IntAnd[List[Char]](10, "abce".toList)) == IntAnd(10, true))
    }

    "traverseLocality" in {
      assert(over(traverseLocality.f[Identity])(_.toUpperCase)(Locality("abc", "def", "ghi")) == Locality("ABC", "DEF", "GHI"))
    }

    "intOrIntP" in {
      assert(over(intOrIntP.f[Function1, Identity])(_ * 10)(IntOrIs(3)) == IntOrIs(30))
      assert(over[IntOr[String], IntOr[String], Int, Int](intOrIntP.f[Function1, Identity])(_ * 10)(IntOrIsNot[String]("abc")) == IntOrIsNot("abc"))
    }

    "intOrLengthEven" in {
      assert(intOrLengthEven(IntOrIsNot("abc".toList)) == IntOrIsNot(false))
      assert(intOrLengthEven(IntOrIsNot("abcd".toList)) == IntOrIsNot(true))
      assert(intOrLengthEven(IntOrIs(10)) == IntOrIs(10))
    }
  }
}
